'''This file contains chemical bonding analysis tools for
analyzing the results based on  GPAW's LCAO mode calculations. Currently
the COOP as well as COHP population analyses are available. Details
can be found:  R. Dronskowski: Computational Chemistry of
Solid State Materials, A. Grechnev, J. Phys. Condens. Matter, 15, 7751 (2003),
Hughbanks, T.; Hoffmann, R., J. Am. Chem. Soc., 1983, 105, 3528,
Dronskowski, R.; Blochl, P. E., J. Phys. Chem., 1993, 97, 8617.
'''

import numpy as np
from math import sqrt, pi
from gpaw.utilities.tools import tri2full
from gpaw.lcao.tools import *
from numpy import pi

from gpaw.fd_operators import Gradient
from gpaw.lfc import LocalizedFunctionsCollection as LFC

class Crystal_orbital_analysis:

    def __init__(self, calc, width=.1, npts = 2001):

        '''Compute the crystal orbital population
        based on a LCAO calculation using GPAW.
        You can also use a grid-based calculation but
        this needs to be projected on a LCAO basis set like:
        calc_lcao = GPAW('grid_calculation.gpw', mode='lcao',
        basis='dzp', fixdensity=True,
        occupations = FermiDirac(0.01,fixmagmom=True))

        Currently only for spinpolarized calculations
        coop = Crystal_orbital_analysis(calc)
        To plot: energies = coop.get_energies()
        COOP0, COOP1 = coop.get_COOP(calc)'''

        #Initialize the calculator properly
        self.calc = calc
        system = self.calc.get_atoms()
        self.calc.set_positions(system)
        self.calc.wfs.eigensolver.iterate(self.calc.hamiltonian, self.calc.wfs)

        self.npts = npts
        self.width = width

        self.nspins = self.calc.wfs.nspins
        self.nkpts = self.calc.wfs.kd.nibzkpts
        self.nbands = self.calc.get_number_of_bands()
        self.n_ao = 0

        for a in range(len(self.calc.get_atoms())):
            self.n_ao += len(get_bfi(self.calc,[a]))

        self.fermi_level = self.calc.get_fermi_level()
        self.eigenvalues_spin0kn = np.array([self.calc.get_eigenvalues(kpt=k,spin=0) for k in range(self.nkpts)])
        self.eigenvalues_spin1kn = np.array([self.calc.get_eigenvalues(kpt=k,spin=1) for k in range(self.nkpts)])
        #self.eigenvalues_spin0kn -=  self.calc.get_fermi_level()
        #self.eigenvalues_spin1kn -=  self.calc.get_fermi_level()
        emin = np.minimum(self.eigenvalues_spin0kn.min(), self.eigenvalues_spin1kn.min()) - 5 * self.width
        emax = np.maximum(self.eigenvalues_spin0kn.max(), self.eigenvalues_spin1kn.max()) + 5 * self.width

        self.energies = np.linspace(emin, emax, npts)

    def get_energies(self):
        '''return the array of energies (relative to Fermi level) used to sample the DOS'''
        return self.energies


    def delta(self,energy):
        '''return a Gaussian with width centered at energy'''
        x = -((self.energies-energy) / self.width)**2
        return np.exp(x) / (sqrt(pi) * self.width)

    def get_COOP(self):
        '''Compute the spin dependent Crystal Orbital Overlap Population (COOP) matrices
        COOP_uv(e) =sum(i,k)[f_i(k)*c_ui(k)*c_vi(k)*S_uv(k)*d(e-e_i(k))]
        where d(e-e_i(k)) is dirac delta function. This
        gives the COOP elements with respect to band and k point
        and the corresponding energies

        The function returns two (spin-dependent) COOP matrices
        '''

        coop_spin0 = np.zeros([self.nkpts,self.nbands])
        coop_spin1 = np.zeros([self.nkpts,self.nbands])

        assert(len(self.calc.wfs.kpt_u) == 2*self.nkpts)
        for kpt in self.calc.wfs.kpt_u:
            S_MM_k_s = self.calc.wfs.S_qMM[kpt.q] #overlap matrix
            C_MM_k_s = kpt.C_nM # weights for AOs at kpt
            f_MM_k_s = kpt.f_n # population of bands
            spin_s = kpt.s #spin index
            kpt_k = kpt.k # kpt index

            '''build the COOP_uvi elements'''

            for n in range(self.nbands):
                for u in range(self.n_ao):
                    for v in range(self.n_ao):
                        '''Remove the diagonal overlap elements'''
                        if v == u :
                            pass
                        else:
                            S_uv = S_MM_k_s[u][v]
                            '''use either the occupation weighted or pure COOP elements'''
                            #c_uvn = S_uv*np.conjugate(C_MM_k_s[n][u])*C_MM_k_s[n][v]*kpt.weight
                            c_uvn = S_uv*C_MM_k_s[n][u]*C_MM_k_s[n][v]
                            if spin_s == 0:
                                coop_spin0[kpt_k][n] +=  c_uvn.real
                            elif spin_s == 1:
                                coop_spin1[kpt_k][n] +=  c_uvn.real

        COOP_spin0 = np.zeros(self.npts)
        COOP_spin1 = np.zeros(self.npts)


        '''positive values for spin0, negative for spin1 '''

        for kpt in self.calc.wfs.kpt_u:
            kpt_k = kpt.k
            for c in range(self.nbands):
                COOP_spin0 += coop_spin0[kpt_k][c]*self.delta(self.eigenvalues_spin0kn[kpt_k][c])
                COOP_spin1 -= coop_spin1[kpt_k][c]*self.delta(self.eigenvalues_spin1kn[kpt_k][c]) # negative for spin1


        return COOP_spin0, COOP_spin1

    def get_COHP(self):
        '''Compute the spin dependent Crystal Orbital Hamiltonian Population (COHP) matrices
        COHP_uv(e) =sum(i,k)[f_i(k)*c_ui(k)*c_vi(k)*H_uv(k)*d(e-e_i(k))]
        where d(e-e_i(k)) is dirac delta function. This
        gives the COHP elements with respect to band and k point
        and the corresponding energies

        The function returns two (spin-dependent) COHP matrices
        '''

        cohp_spin0 = np.zeros([self.nkpts,self.nbands])
        cohp_spin1 = np.zeros([self.nkpts,self.nbands])
        H_skMM, S_kMM = get_lcao_hamiltonian(self.calc) # the hamiltonian and overlap matrices

        assert(len(self.calc.wfs.kpt_u) == 2*self.nkpts)
        for kpt in self.calc.wfs.kpt_u:
            C_MM_k_s = kpt.C_nM # weights for AOs at kpt
            f_MM_k_s = kpt.f_n # population of bands
            spin_s = kpt.s #spin index
            kpt_k = kpt.k # kpt index

            '''build the COOP_uvi elements'''

            for n in range(self.nbands):
                for u in range(self.n_ao):
                    for v in range(self.n_ao):
                        '''Remove the diagonal overlap elements'''
                        if v == u :
                            pass
                        else:
                            H_uv = H_skMM[spin_s][kpt_k][u][v]

                            '''use either the occupation weighted or pure COHP elements'''

                            c_uvn = H_uv*np.conjugate(C_MM_k_s[n][u])*C_MM_k_s[n][v]*kpt.weight
                            #c_uvn = H_uv*C_MM_k_s[n][u]*C_MM_k_s[n][v]
                            if spin_s == 0:
                                cohp_spin0[kpt_k][n] +=  c_uvn.real
                            elif spin_s == 1:
                                cohp_spin1[kpt_k][n] +=  c_uvn.real

        COHP_spin0 = np.zeros(self.npts)
        COHP_spin1 = np.zeros(self.npts)

        '''positive values for spin0, negative for spin1 '''

        for kpt in self.calc.wfs.kpt_u:
            kpt_k = kpt.k
            for c in range(self.nbands):
                COHP_spin0 += cohp_spin0[kpt_k][c]*self.delta(self.eigenvalues_spin0kn[kpt_k][c])
                COHP_spin1 -= cohp_spin1[kpt_k][c]*self.delta(self.eigenvalues_spin1kn[kpt_k][c]) # negative for spin1

        return -COHP_spin0, -COHP_spin1

    def get_pCOP(self, alist, projected_atoms=None, pCOP_type = 'COOP'):
        '''get the COOP/COHP projected on the atoms given in alist.
        Unlike regular DOS, total_pCOHP can be used to probe binding/antibinding states
        between the total system and specified subset of atoms

        alist = list of atom indices to project on
        projected_atoms = list of atoms to be projected on atoms in alist
        pCOP_type = 'COHP' gives projected COHP whereas 'COOP' gives COOP type projections '''


        self.alist = alist
        self.n_atoms = len(self.alist)

        if projected_atoms is None:
            proj = range(self.n_ao)

        else:
            for a in projected_atoms:
                proj += get_bfi(self.calc,[a])

        proj_a = np.array(proj)

        H_skMM, S_kMM = get_lcao_hamiltonian(self.calc) # the hamiltonian and overlap matrices

        pcop_spin0 = np.empty([self.n_atoms,self.nkpts,self.nbands])
        pcop_spin1 = np.empty([self.n_atoms,self.nkpts,self.nbands])

        for a in self.alist:

            ''' Indices M1-M2 are the AOs belonginging to atom a'''
            proj_AOs = get_bfi(self.calc,[a])

            for kpt in self.calc.wfs.kpt_u:
                C_MM_k_s = kpt.C_nM # weights for AOs at kpt
                f_MM_k_s = kpt.f_n # population of bands
                spin_s = kpt.s #spin index
                kpt_k = kpt.k # kpt index

                '''build the COOP_uvi elements'''

                for n in range(self.nbands):
                    for v in proj_a: # choose the AOs from which project, by default all atoms, speficied by projected_atoms
                        for u in proj_AOs: # the AOs to project on
                                '''Remove the diagonal overlap elements to describe covalent bonds'''
                                if v == u :
                                    pass
                                else: # Choose COOP or COHP projections
                                    if pCOP_type == 'COOP':
                                        COP_uv == S_skMM[kpt_k][u][v]
                                    else:
                                        COP_uv = H_skMM[spin_s][kpt_k][u][v]

                                    c_uvn = COP_uv*np.conjugate(C_MM_k_s[n][u])*C_MM_k_s[n][v]*kpt.weight
                                    #c_uvn = COP_uv*C_MM_k_s[n][u]*C_MM_k_s[n][v]

                                    if spin_s ==0:
                                         pcop_spin0[a][kpt_k][n] +=  c_uvn.real
                                    elif spin_s ==1:
                                         pcop_spin1[a][kpt_k][n] +=  c_uvn.real

            COP_spin0 = np.zeros([self.n_atoms,self.npts])
            COP_spin1 = np.zeros([self.n_atoms,self.npts])


            '''positive values for spin0, negative for spin1 '''
            for a in range(len(alist)):
                for kpt in self.calc.wfs.kpt_u:
                    kpt_k = kpt.k
                    for n in range(self.nbands):
                        COP_spin0[a] += pcop_spin0[a][kpt_k][n]*self.delta(self.eigenvalues_spin0kn[kpt_k][n])
                        COP_spin1[a] -= pcop_spin1[a][kpt_k][n]*self.delta(self.eigenvalues_spin1kn[kpt_k][n])


            if pCOP_type == 'COOP':
                return COP_spin0, COP_spin1

            elif pCOP_type=='COHP':
                return -COP_spin0, -COP_spin1

    def get_molCOP(self, ads_calc, alist=[], projected_atoms = None,
            ads_orbitals=[-1], surface_bands = None, molCOP_type= 'COOP'):
        ''' Computes the density of state projected on an MOLECULAR ORBITAL.
        ads_calc defines the molecule to project on and partition
        the system adsorbate and surface. normal calc given for the LCAO_ANALYSIS
        class is the surface and adsorbate calculator

        alist defines which atoms from surface/adsorbate system form the
        adsorbate (partition between surface and adsorbate)

        projected_atoms = which atoms from surface+adsorbate to project on adsorbate

        orbital defines on which adsorbate molecular orbital the surface states are projected

        ADSORBATE ATOMS MUST BE IN THE SAME ORDER IN BOTH ADS_CALC AND CALC!

        Molecule states |i), surface states |j), a = basis functions on molecule, b = basis on surface
        Then the molDOS is
        p_i (e) = sum(j)[(j|i)(i|j)] --> (i|j) = sum(a,b) = conj(c_ia)*c_jb*S_ab

        Bonding / antibonding character is based on sign((i|j)) --> for COOP negative is antibonding,
        positive is bonding
        '''

        if surface_bands is None:
            surface_bands = self.nbands

        if projected_atoms is None:
            proj = range(self.n_ao)

        else:
            for a in projected_atoms:
                proj += get_bfi(self.calc,[a])

        proj_a = np.array(proj)
        # AO indices of the adsorbate in surface+adsorbate system
        ads_aos = 0
        for a in alist:
            ads_aos += len(get_bfi(self.calc,[a]))

            H_skMM_surf, S_kMM_surf = get_lcao_hamiltonian(self.calc) # the hamiltonian and overlap matrices

            molcop_spin0 = np.zeros([self.nkpts,surface_bands,ads_orbitals])
            molcop_spin1 = np.zeros([self.nkpts,surface_bands,ads_orbitals])

            for kpt_surf, kpt_ads in zip(self.calc.wfs.kpt_u, ads_calc.wfs.kpt_u):
                C_MM_k_s_surf = kpt_surf.C_nM # weights for AOs at kpt
                f_MM_k_s_surf = kpt_surf.f_n # population of bands
                spin_s_surf = kpt_surf.s #spin index
                kpt_k_surf = kpt_surf.k # kpt index

                for n in range(surface_bands): #surface bands
                    for u in range(proj_a): #chosen surface atoms and their basis orbitals
                        for v in range(proj_a):
                            for i in ads_orbitals:
                                C_MM_k_s_ads = kpt_ads.C_nM[i] # weights for AOs at kpt
                                for a in ads_aos:
                                    for b in ads_aos:
                                        # overlap
                                        S_ua = S_kMM_surf[kpt_k][u][a]
                                        # Choose COOP or COHP projections
                                        if molCOP_type == 'COOP':
                                            COP_uv == S_skMM[kpt_k][b][v]
                                        else:
                                            COP_uv = H_skMM[spin_s][kpt_k][b][v]

                                        COP_element = S_ua * COP_uv * np.conjugate(C_MM_k_s_ads[a]) * np.conjugate(C_MM_k_s[n][u]) * \
                                        C_MM_k_s_ads[b] * C_MM_k_s[n][v] * kpt_weight

                                        if spin_s ==0:
                                            molcop_spin0[kpt_k][n][i] +=  COP_element
                                        elif spin_s ==1:
                                            molcop_spin1[a][kpt_k][i] -=  COP_element

        COP_spin0 = np.zeros([self.npts])
        COP_spin1 = np.zeros([self.npts])

        '''positive values for spin0, negative for spin1 '''
        for kpt in self.calc.wfs.kpt_u:
            kpt_k = kpt.k
            for n in range(self.nbands):
                COP_spin0 += pcop_spin0[kpt_k][n]*self.delta(self.eigenvalues_spin0kn[kpt_k][n])
                COP_spin1 -= pcop_spin1[kpt_k][n]*self.delta(self.eigenvalues_spin1kn[kpt_k][n])


        if molCOP_type == 'COOP':
            return COP_spin0, COP_spin1

        elif molCOP_type=='COHP':
            return -COP_spin0, -COP_spin1

def get_mulliken_population(calc, alist):

    '''Mulliken charges from a list of atom indices (a_list).'''
    import numpy as np

    Q_a = {}

    for a in alist:
        Q_a[a] = 0.0

    for kpt in calc.wfs.kpt_u:
        S_MM = calc.wfs.S_qMM[kpt.q]
        rho_MM = np.empty((S_MM.shape[0], S_MM.shape[0]) ,calc.wfs.dtype)
        calc.wfs.calculate_density_matrix(kpt.f_n, kpt.C_nM, rho_MM)
        Q_M = np.dot(rho_MM, S_MM).diagonal()

        for a in alist:
            M1 = calc.wfs.basis_functions.M_a[a]
            M2 = M1 + calc.wfs.setups[a].niAO
            Q_a[a] += np.sum(Q_M[M1:M2]).real

    return Q_a

'''def get_mulliken_gop(alist, calc):
    #get the Mulliken Gross Orbital Populations for atoms in alist

    #Should be improved for general elements but haven't found a tool to
    #to orbital symmetries/types for different basis sets. Currently these
    #have to be fed by hand

    import numpy as np
    Q_a = {}
    GOP = {}
    population_spin0 =0.
    population_spin1=0.

    for a in alist:
        Q_a[a] = 0.0

    GOP_table_spin0 = {}
    GOP_table_spin1 = {}

       	for a in alist:
              	GOP_table_spin0[a] =0.
            	GOP_table_spin1[a] =0.

        for kpt in calc.wfs.kpt_u:

	        S_MM = calc.wfs.S_qMM[kpt.q]

                rho_MM = np.empty((S_MM.shape[0],S_MM.shape[0]), calc.wfs.dtype)

                calc.wfs.calculate_density_matrix(kpt.f_n, kpt.C_nM, rho_MM)
                Q_M = rho_MM*np.transpose(S_MM) #population matrix
               	GOPs = np.sum(Q_M.real, axis=0) #sum up column elements to form gross orbital populations

                for a in alist:
                       	atom = calc.atoms[a].symbol
                       	M1 = calc.wfs.basis_functions.M_a[a]

	                M2 = M1 + calc.wfs.setups[a].niAO

        	        if kpt.s==0:
                                if (M2-M1)==5: #the number of basis functions for first row elements in DZP
                                       	GOP_table_spin0[a] = ('%s_%d_spin%d'%(atom,a,kpt.s),'1s:%4.3f'%GOPs[M1],'1s_split:%4.3f'%GOPs[M1+1],'2py_pol:%4.3f'%GOPs[M1+2],'2pz_pol:%4.3f'%GOPs[M1+3],
                                       	'2px_pol:%4.3f'%GOPs[M1+4], 'Mulliken population: %4.3f'%np.sum(GOPs[M1:M2]))

                               	elif (M2-M1)==15:#the number of basis functions for third row transition metals in DZP
                                       	GOP_table_spin0[a] = ('%s_%d_spin%d'%(atom,a,kpt.s),'4s:%4.3f'%GOPs[M1],'3d_xy:%4.3f'%GOPs[M1+1],'3d_yz:%4.3f'%GOPs[M1+2],'3d_z^2-r^2:%4.3f'%GOPs[M1+3],
                                       	'3d_xz:%4.3f'%GOPs[M1+4],'3d_x^2-y^2:%4.3f'%GOPs[M1+5],'4s_split:%4.3f'%GOPs[M1+6],'3d_xy_split:%4.3f'%GOPs[M1+7],
                                       	'3d_yz_split:%4.3f'%GOPs[M1+8],'3d_z^2-r^2_split:%4.3f'%GOPs[M1+9],'3d_xz_split:%4.3f'%GOPs[M1+10],'3d_x^2-y^2_split:%4.3f'%GOPs[M1+11],
                                       	'3py_pol:%4.3f'%GOPs[M1+12],'3pz_pol:%4.3f'%GOPs[M1+13],'3px_pol:%4.3f'%GOPs[M1+14],' Mulliken population: %4.3f'%np.sum(GOPs[M1:M2]))

	                        elif (M2-M1)==13: # for the second row atoms with p-electrons
        	                         GOP_table_spin0[a] = ('%s_%d_spin%d'%(atom,a,kpt.s),'2s:%4.3f'%GOPs[M1],'2py:%4.3f'%GOPs[M1+1],'2pz:%4.3f'%GOPs[M1+2],'2px:%4.3f'%GOPs[M1+3],
                                        '2s_split:%4.3f'%GOPs[M1+4],'2py_split:%4.3f'%GOPs[M1+5],'2pz_split:%4.3f'%GOPs[M1+6],'2px_split:%4.3f'%GOPs[M1+7],
                       	                '3d_xy_split:%4.3f'%GOPs[M1+8],'3d_yz_split:%4.3f'%GOPs[M1+9],'3d_z^2-r^2_split:%4.3f'%GOPs[M1+10],
                               	        '3d_xz_split:%4.3f'%GOPs[M1+11],'3d_x^2-y^2_split:%4.3f'%GOPs[M1+12],' Mulliken population: %4.3f'%np.sum(GOPs[M1:M2]))

        	                population_spin0  += np.sum(GOPs[M1:M2])

                        elif kpt.s==1:
                       	        if (M2-M1)==5: #the number of basis functions for first row elements in DZP
                              	        GOP_table_spin1[a] = ('%s_%d_spin%d'%(atom,a,kpt.s),'1s:%4.3f'%GOPs[M1],'1s_split:%4.3f'%GOPs[M1+1],'2py_pol:%4.3f'%GOPs[M1+2],'2pz_pol:%4.3f'%GOPs[M1+3],
                                       	'2px_pol:%4.3f'%GOPs[M1+4], 'Mulliken population: %4.3f'%np.sum(GOPs[M1:M2]))

                               	elif (M2-M1)==15:#the number of basis functions for third row transition metals in DZP
                                       	GOP_table_spin1[a] = ('%s_%d_spin%d'%(atom,a,kpt.s),'4s:%4.3f'%GOPs[M1],'3d_xy:%4.3f'%GOPs[M1+1],'3d_yz:%4.3f'%GOPs[M1+2],'3d_z^2-r^2:%4.3f'%GOPs[M1+3],
                                       	'3d_xz:%4.3f'%GOPs[M1+4],'3d_x^2-y^2:%4.3f'%GOPs[M1+5],'4s_split:%4.3f'%GOPs[M1+6],'3d_xy_split:%4.3f'%GOPs[M1+7],
                                       	'3d_yz_split:%4.3f'%GOPs[M1+8],'3d_z^2-r^2_split:%4.3f'%GOPs[M1+9],'3d_xz_split:%4.3f'%GOPs[M1+10],'3d_x^2-y^2_split:%4.3f'%GOPs[M1+11],
                                       	'3py_pol:%4.3f'%GOPs[M1+12],'3pz_pol:%4.3f'%GOPs[M1+13],'3px_pol:%4.3f'%GOPs[M1+14],' Mulliken population: %4.3f'%np.sum(GOPs[M1:M2]))

	                        elif (M2-M1)==13: # for the second row atoms with p-electrons
                                        GOP_table_spin1[a] = ('%s_%d_spin%d'%(atom,a,kpt.s),'2s:%4.3f'%GOPs[M1],'2py:%4.3f'%GOPs[M1+1],'2pz:%4.3f'%GOPs[M1+2],'2px:%4.3f'%GOPs[M1+3],
               	                        '2s_split:%4.3f'%GOPs[M1+4],'2py_split:%4.3f'%GOPs[M1+5],'2pz_split:%4.3f'%GOPs[M1+6],'2px_split:%4.3f'%GOPs[M1+7],
                       	                '3d_xy_split:%4.3f'%GOPs[M1+8],'3d_yz_split:%4.3f'%GOPs[M1+9],'3d_z^2-r^2_split:%4.3f'%GOPs[M1+10],
                               	        '3d_xz_split:%4.3f'%GOPs[M1+11],'3d_x^2-y^2_split:%4.3f'%GOPs[M1+12],' Mulliken population: %4.3f'%np.sum(GOPs[M1:M2]))
                               	population_spin1 += np.sum(GOPs[M1:M2])

	return GOP_table_spin0, GOP_table_spin1, 'Mulliken from GOPs for spin 0: ',population_spin0, 'Mulliken from GOPs for spin 1: ',population_spin1, 'Total population of selected atoms : ', population_spin0+population_spin1
'''
def get_mulliken_bond_order(calc, atom_pairs):
    ''' get the BO (no spin or k-point dependency)  from LCAO density matrix as described
    in Mulliken, J. Phys. Chem., 1955.

    Mulliken BO has 'poorly' defined and does not yield integer BOs. See critic by
    I. Mayer J. Comp. Chem, 28, 204 (2006)

    atom_pairs=[[a1,a2],[a3,a5],[a4,a6]]  list of  atomic pairs is required'''

    import numpy as np

    BO = {}

    for a in range(len(atom_pairs)):
        BO[a] = 0.0

    for kpt in calc.wfs.kpt_u:
        S_MM = calc.wfs.S_qMM[kpt.q]

        rho_MM = np.empty((S_MM.shape[0],S_MM.shape[0]), calc.wfs.dtype)
        calc.wfs.calculate_density_matrix(kpt.f_n, kpt.C_nM, rho_MM)
        Q_M = (rho_MM*np.transpose(S_MM)).real #population matrix


        for a, i in zip(atom_pairs,range(len(atom_pairs))):
            u1 = calc.wfs.basis_functions.M_a[a[0]]
            u2 = u1 + calc.wfs.setups[a[0]].niAO
            v1 = calc.wfs.basis_functions.M_a[a[1]]
            v2 = v1 + calc.wfs.setups[a[1]].niAO
            BO[i] += np.sum(Q_M[u1:u2,v1:v2]).real

        return BO


def get_mayer_bond_order(calc, atom_pairs):
    ''' Get BOs as described by Mayer. I. Mayer J. Comp. Chem, 28, 204 (2006)

    atom_pairs=[[a1,a2],[a3,a5],[a4,a6]]  list of  atomic pairs is required'''

    import numpy as np

    BO = {}

    for a in range(len(atom_pairs)):
        BO[a] = 0.0

    for kpt in calc.wfs.kpt_u:
        S_MM = calc.wfs.S_qMM[kpt.q]

        rho_MM = np.empty((S_MM.shape[0],S_MM.shape[0]), calc.wfs.dtype)
        calc.wfs.calculate_density_matrix(kpt.f_n, kpt.C_nM, rho_MM)

        Q_M = np.dot(rho_MM, S_MM)

        for a,i in zip(atom_pairs,range(len(atom_pairs))): # a pair index, i BO index

            u1 = calc.wfs.basis_functions.M_a[a[0]]
            u2 = u1 + calc.wfs.setups[a[0]].niAO

            v1 = calc.wfs.basis_functions.M_a[a[1]]
            v2 = v1 + calc.wfs.setups[a[1]].niAO

            for u in range(u1,u2):
                for v in range(v1,v2):
                    BO[i] += (2*Q_M[u][v]*Q_M[v][u]).real
        return BO

def real_space_covalent_bond_order(calc, atoms=[0,1]):
    '''Covalent bond orders from electron density and Hirschfeld charge partition
    modification of DOI: 10.1021/ja00011a014
    BO_AB = sum_i f_i <i|i>_A<i|i>_B'''
    from gpaw.analyse import hirshfeld
    from gpaw.utilities.ps2ae import PS2AE

    h = HirshfeldPartitioning(calc)
    gd = calc.density.gd
    wa = h.get_weight(atoms[0])
    wb = h.get_weight(atoms[1])
    # interpolate to fine grid
    psi_A = PS2AE(calc, h = 0.05)
    wa_g = psi_A.interpolate_weight(wa,gd)
    wb_g = psi_A.interpolate_weight(wb,gd)

    # number of bands
    ns = calc.wfs.nspins
    occup_ks = np.zeros((len(calc.wfs.kd.weight_k),ns),dtype = int)

    for k in range(calc.wfs.kd.nibzkpts):
        for s in range(ns):
            f_n = calc.get_occupation_numbers(kpt=k, spin=s)
            f_N = f_n > 1e-6
            occup_ks[k][s] += f_N.sum()

    BO = 0. #bond order between A and B
    #loop over kpt, spin and bands
    for spin in range(ns):
        for k in range(calc.wfs.kd.nibzkpts):
            f_n = calc.get_occupation_numbers(kpt=k, spin=s)
            for i in n_occup_k[k][spin]:
                psi_kA = psi_A.get_wave_function(n = i, k = k, s = spin,ae = True)
                n_iA = psi_A.gd.integrate(psi_kA.conj(), wa_g*psi_kB, global_integral= True)
                n_iB = psi_A.gd.integrate(psi_kA.conj(), wb_g*psi_kB, global_integral= True)
                BO += f_n[i]**2 * n_iA * n_iB
    return BO


def get_mayer_valence_index(calc, alist):
    ''' Compute the atomic valences and free valencesas defined by Mayer,
    I. Mayer J. Comp. Chem, 28, 204 (2006), eq 64, 66, 84 using Mulliken partitioning
    Note, only the spin independent valencency makes any sense '''


    import numpy as np

    nspins = calc.get_number_of_spins()
    nkpts = len(calc.wfs.weight_k)
    nbands = calc.get_number_of_bands()
    n_ao = np.sum([calc.wfs.setups[a].niAO for a in range(len(calc.get_atoms()))]) #number of basis functions

    F_a = {}
    V_a = {}
    BO_a = {}

    for a in range(len(alist)):
        F_a[a] = 0.
        V_a[a] = 0.
        BO_a[a] = 0. #Total bond order of A, e.g. to sum of all bond orders of A-B pairs, A!=B

        P_aS = np.zeros((n_ao,n_ao), calc.wfs.dtype) #Spin 0 density matrix . overlap matrix
        P_bS = np.zeros((n_ao,n_ao), calc.wfs.dtype)

        ''' Build the P_aS and P_bS matrices'''
    for kpt in calc.wfs.kpt_u:
        S_MM = calc.wfs.S_qMM[kpt.q]
        rho_MM = np.empty((S_MM.shape[0],S_MM.shape[0]),calc.wfs.dtype)
        calc.wfs.calculate_density_matrix(kpt.f_n, kpt.C_nM, rho_MM)

        PS = np.dot(rho_MM, S_MM)


        if kpt.s == 0:
            P_aS += PS
        elif kpt.s ==1:
            P_bS += PS

        ''' Build the bond orders needed for calculating valences later on'''

        for a in alist:
            u1 = calc.wfs.basis_functions.M_a[a]
            u2 = u1 + calc.wfs.setups[a].niAO
            B_atoms = range(n_ao)

            for bf in range(u1,u2): # remove basis functions belonging to atom a
                B_atoms.remove(bf)

            for u in B_atoms:
                for v in range(u1, u2):
                    BO[a] += (2*PS[u][v]*PS[v][u]).real

        ''' Compute free valences F_a '''

        for a in alist:
            v1 = calc.wfs.basis_functions.M_a[a]
            v2 = v1 + calc.wfs.setups[a].niAO

            for u in range(n_ao):
                for v in range(v1,v2):
                    F_a[a] += ( (P_aS[u][v]-P_bS[u][v])*(P_aS[v][u]-P_bS[v][u]) ).real

                ''' Compute valences V_a from F_a using Mayer bond orders '''
            V_a[a] = F_a[a]+BO[a]
        return 'Free valences: ', F_a, '\nValences: ', V_a

def get_lowdin_population(calc, alist, return_weight = False, return_kpts = False):
    ''' Lowdin's method for computing atomic charges.
    If return_weight=True returns also Lowdin weight matrix
    If return_kpts = True charges and weights at each kpts is returned
    Also a method for obtaining the  orthonormalized basis functions could be implemented'''

    from numpy.linalg import svd #singular value decomposition
    import numpy as np

    nkpts = len(calc.wfs.weight_k)
    nspins = calc.get_number_of_spins()
    natoms = len(alist)

    # array of nspins * natoms * nkpts

    Q_a = np.zeros((nspins,natoms, nkpts))

    if return_weight == True:
        w_a = [[ 0.0 for k in range(nkpts)] for s in range(nspins)]

    for kpt, k_index in zip(calc.wfs.kpt_u, nkpts):
        S_MM = calc.wfs.S_qMM[kpt.q]
        rho_MM = np.empty((S_MM.shape[0],S_MM.shape[0]),calc.wfs.dtype)
        calc.wfs.calculate_density_matrix(kpt.f_n, kpt.C_nM, rho_MM)

        U,D,V =np.linalg.svd(S_MM, full_matrices=True) #V=U(+)
        s = np.diag(D**(0.5))

        S_square = np.dot(U, np.dot(s,V)) #S^(1/2)

        Q_M = np.diag(np.dot(S_square, np.dot(rho_MM, S_square))).real #diagonal of S^(1/2)*rho*S^(1/2)

        for a in alist:

            M1 = calc.wfs.basis_functions.M_a[alist[a]]
            M2 = M1 + calc.wfs.setups[alist[a]].niAO
            Q_a[kpt.s][a][k_index] = np.sum(Q_M[M1:M2]) #trace of S^(1/2)*rho*S^(1/2) for specific spin, atom,kpt
            if return_weight == True:
                w_ka = np.dot(S_square[:,M1:M2], S_square[M1:M2,:]) # sum(u (- k [S_lu * S_uv])
                w_a[kpt.s][k_index].append(w_ka)

    if return_weight == False and return_kpts == False:
         #return Lowdin charge for each spin and atom, summed over kpts
        Q = np.zeros((nspins,natoms))
        for s in range(nspins):
            for a in range(natoms):
                Q[s][a] = np.sum(Q_a[s][a][:])

    elif return_weight == True and return_kpts == False:
        return w_a, Q_a


def charge_decomposition_analysis(AB_calc, A_calc, B_calc):
    ''' Calculate the CDA analysis using fragment molecular orbitals.

    An interacting system AB is split to fragments A and B.

    Contributions to bonding, back-bonding and repulsion between
    the fragment are computed

    Atoms and AO basis sets should be in the same order in the AB complex
    and fragments. A and B should be in the same positions as in AB as the
    method is not rotationally invariant

    Also the spin state of AB must be the same as A+B.

    For details: S. Dapprich, G. Frenkin, J. Phys. Chem., 99, 9352 (1995)
    S. Gorelsky, E. Solomon, Theor. Chem. Account., 119, 57 (2008)'''

    import numpy as np
    from numpy import linalg as LA
    from scipy.linalg import block_diag

    nspins = calc.get_number_of_spins()
    nkpts = len(calc.wfs.weight_k)

    nbands_A = A_calc.get_number_of_bands()
    n_ao_A = np.sum([A_calc.wfs.setups[a].niAO for a in range(len(A_calc.get_atoms()))]) #number of basis functions on fragment A
    spin_A = calc_A.get_magnetic_moment()

    nbands_B = B_calc.get_number_of_bands()
    n_ao_B = np.sum([B_calc.wfs.setups[a].niAO for a in range(len(B_calc.get_atoms()))]) #number of basis functions on fragment B
    spin_B = calc_B.get_magnetic_moment()

    nbands_AB = AB_calc.get_number_of_bands()
    n_ao_AB = np.sum([AB_calc.wfs.setups[a].niAO for a in range(len(AB_calc.get_atoms()))]) #number of basis functions on complex AB
    spin_AB = calc_AB.get_magnetic_moment()

    ''' Matrices for spin dependent bonding, backbonding and repulsion terms'''

    d_i_spin0 = np.empty((nbands_AB))
    d_i_spin1 = np.empty((nbands_AB))

    b_i_spin0 = np.empty((nbands_AB))
    b_i_spin1 = np.empty((nbands_AB))

    r_i_spin0 = np.empty((nbands_AB))
    r_i_spin1 = np.empty((nbands_AB))

    if (n_ao_A + n_ao_B) != n_ao_AB or (nbands_A+nbands_B) != nbands_AB or np.absolute(spin_A + spin_B - spin_AB) >0.5 :
        raise ValueError('Number of bands, basis orbitals or spin state for complex does not match the fragments!')


    for kpt_A, kpt_B, kpt_AB in zip(calc_A.wfs.kpt_u, calc_B.wfs.kpt_u, calc_AB.wfs.kpt_u):
        C_MM_k_s_A = kpt_A.C_nM # weights for AOs at kpt
        f_MM_k_s_A = kpt_A.f_n # population of bands

        C_MM_k_s_B = kpt_B.C_nM # weights for AOs at kpt
        f_MM_k_s_B = kpt_B.f_n # population of bands

        C_MM_k_s_AB = kpt_AB.C_nM # weights for AOs at kpt
        f_MM_k_s_AB = kpt_AB.f_n # population of bands
        S_MM_AB_LCAO = calc_AB.wfs.S_qMM[kpt.q] # complex overlap matrix in the LCAO basis
        w_k_AB = kpt_AB.weight
        spin = kpt_AB.s

        ''' Block diagonal matrix for the direct sum of FMOs'''

        FMO_k_s = block_diag(C_MM_k_s_A,C_MM_k_s_B)
        inv_FMO_k_s = LA.inv(FMO_k_s) # inv_FMO_k_s, the inverse of FMO coefficient matrix

        '''Unitary transformation of C_MM_k_s_AB to FMO basis'''

        FMO_AB = np.dot(inv_FMO_k_s, C_MM_k_s_AB)

        ''' Similarity transformation of the complex LCAO overlap matrix to FMO basis
        S(FMO) = C(FMO)^-1 S(LCAO) C(FMO) (C is unitary)'''

        dagger_FMO_k_s = np.conjugate(np.transpose(FMO_k_s)) # adjoint of FMO_k_s
        S_MM_AB_FMO = np.dot(dagger_FMO_k_s, np.dot(S_MM_AB_LCAO, FMO_k_s))

        ''' Spin dependent bonding contributions, averaged over k-points'''
        for i in range(nbands_AB):
            for m in range(nbands_A):
                for n in range(nbands_B):
                    #''' Bonding '''
                    if f_MM_k_s_A[m] >= 0.5 and f_MM_k_s_B[n]<=0.5:
                        if spin ==0:
                            d_i_spin0[i] += w_k_AB * f_MM_k_s_AB[i] * np.conjugate(FMO_AB[m][i]) * FMO_AB[n][i] * S_MM_AB_FMO[m][n]
                        elif spin ==1:
                            d_i_spin1[i] += w_k_AB * f_MM_k_s_AB[i] * np.conjugate(FMO_AB[m][i]) * FMO_AB[n][i] * S_MM_AB_FMO[m][n]
                    #''' Back-bonding'''
                    elif f_MM_k_s_A[m] <= 0.5 and f_MM_k_s_B[n] >=0.5:
                        if spin ==0:
                            d_i_spin0[i] += w_k_AB * f_MM_k_s_AB[i] * np.conjugate(FMO_AB[n][i]) * FMO_AB[m][i] * S_MM_AB_FMO[n][m]
                        elif spin ==1:
                            d_i_spin1[i] += w_k_AB * f_MM_k_s_AB[i] * np.conjugate(FMO_AB[n][i]) * FMO_AB[m][i] * S_MM_AB_FMO[n][m]
                    #''' Repulsion''
                    else:
                        if spin ==0:
                            d_i_spin0[i] += w_k_AB * f_MM_k_s_AB[i] * np.conjugate(FMO_AB[m][i]) * FMO_AB[n][i] * S_MM_AB_FMO[m][n]
                        elif spin ==1:
                            d_i_spin1[i] += w_k_AB * f_MM_k_s_AB[i] * np.conjugate(FMO_AB[m][i]) * FMO_AB[n][i] * S_MM_AB_FMO[m][n]
    '''Write CDA analysis to a file and return it'''

    CDA_file = open('CDA_analysis.txt','w')

    CDA_file.write('CDA_analysis\n')
    CDA_file.write('Average donation: %4.3f\n'%(np.sum(d_i_spin0)+np.sum(d_i_spin1)))
    CDA_file.write('Spin 0 donation: %4.3f\n' %np.sum(d_i_spin0))
    CDA_file.write('Spin 1 donation: %4.3f\n' %np.sum(d_i_spin1))
    CDA_file.write('Average backdonation: %4.3f\n'%(np.sum(b_i_spin0)+np.sum(b_i_spin1)))
    CDA_file.write('Spin 0 backdonation: %4.3f\n' %np.sum(b_i_spin0))
    CDA_file.write('Spin 1 backdonation: %4.3f\n' %np.sum(b_i_spin1))
    CDA_file.write('Average repulsion: %4.3f\n'%(np.sum(r_i_spin0)+np.sum(r_i_spin1)))
    CDA_file.write('Spin 0 repulsion: %4.3f\n' %np.sum(r_i_spin0))
    CDA_file.write('Spin 1 repulsion: %4.3f\n' %np.sum(r_i_spin1))
    CDA_file.write('*--------------------------------------------------------------------*\n')
    CDA_file.write('Orbital specific CDA parameters')
    CDA_file.write('Orbital		Bonding		Back-bonding		Repulsion\n')
    CDA_file.write('----------------------------------------------------------------------\n')

    for i in range(nbands_AB):
        di = d_i_spin0[i]+d_i_spin1[i]
        bi = b_i_spin0[i]+b_i_spin1[i]
        ri = r_i_spin0[i]+r_i_spin1[i]
        CDA_file.write('%d	       %5.4f		  %5.4f			%5.4f\n'%(i,bi,di,ri))

def get_condensed_fukui_functions(calc_neutral, calc_FF, bond_fukui_functions='None'):
    '''This method uses Mulliken's method for defining to atoms-in-molecules.
    Based on this partition atomically condensed Fukui functions are computed
    using finite differences for approximating the derivatives.

    calc_neutral should charge=0 while for calc_FF charge=-/+ 1 depending on
    will the electrophilic/nucleophilic FF indices are required

    bond_fukui_functions = [[a1,a2],[a3,a4] ... ], list atom pairs for which to compute the bond FF

    For details: P. Bultinck, J. Comp. Chem., 2421 (2013)
         M. Gonzales-Suarez, J. Org. Chem, 77,90 (2011)

    IN PROGRESS.... SINCE MANY YEARS....
    '''


class ElectronDensityAnalysis:
    def __init__(self,paw,ncut=1e-6):
        self.gd = paw.wfs.gd
        self.paw = paw
        self.finegd = paw.density.finegd
        self.gd = paw.density.gd
        self.nspins = paw.density.nspins
        self.density = paw.density

        self.ncut = ncut
        self.spinpol = (self.nspins == 2)

        self.initialize(paw)

    def initialize(self, paw):

        if not paw.initialized:
            raise RuntimeError('PAW instance is not initialized')
        paw.converge_wave_functions()

        self.tauct = LFC(self.gd,
                         [[setup.tauct] for setup in self.density.setups],
                         forces=True, cut=True)
        spos_ac = paw.atoms.get_scaled_positions() % 1.0
        self.tauct.set_positions(spos_ac)

        self.taut_sg = None
        self.nt_grad2_sG = self.gd.empty(self.nspins)
        self.nt_grad2_sg = None

    def interpolate(self):

        self.density.interpolate_pseudo_density()

        if self.taut_sg is None:
            self.taut_sg = self.finegd.empty(self.nspins)
            self.nt_grad2_sg = self.finegd.empty(self.nspins)

        ddr_v = [Gradient(self.finegd, v, n=3).apply for v in range(3)]
        self.nt_grad2_sg[:] = 0.0
        d_g = self.finegd.empty()

        # Transfer the densities from the coarse to the fine grid
        for s in range(self.nspins):
            self.density.interpolator.apply(self.taut_sG[s],
                                            self.taut_sg[s])
            #self.density.interpolator.apply(self.nt_grad2_sG[s],
            #                                self.nt_grad2_sg[s])
            for v in range(3):
                ddr_v[v](self.density.nt_sg[s], d_g)
                self.nt_grad2_sg[s] += d_g**2.0

    def update(self):
        self.taut_sG = self.paw.wfs.calculate_kinetic_energy_density()

        # Add the pseudo core kinetic array
        for taut_G in self.taut_sG:
            self.tauct.add(taut_G, 1.0 / self.paw.wfs.nspins)

        # For periodic boundary conditions
        if self.paw.wfs.kd.symmetry is not None:
            self.paw.wfs.kd.symmetry.symmetrize(self.taut_sG[0], self.paw.wfs.gd)

        self.nt_grad2_sG[:] = 0.0

        d_G = self.gd.empty()

        for s in range(self.nspins):
            for v in range(3):
                self.paw.wfs.taugrad_v[v](self.density.nt_sG[s], d_G)
                self.nt_grad2_sG[s] += d_G**2.0

        # TODO are nct from setups usable for nt_grad2_sG ?

    def get_electronic_localization_function(self, gridrefinement=1,
                                             pad=True, broadcast=True):

        # Returns dimensionless electronic localization function
        if gridrefinement == 1:
            elf_G = self.elf(self.density.nt_sG, self.nt_grad2_sG,
                         self.taut_sG, self.ncut, self.spinpol)
            elf_G = self.gd.collect(elf_G, broadcast)
            if pad:
                elf_G = self.gd.zero_pad(elf_G)
            return elf_G
        elif gridrefinement == 2:
            if self.nt_grad2_sg is None:
                self.interpolate()

            elf_g = self.elf(self.density.nt_sg, self.nt_grad2_sg,
                         self.taut_sg, self.ncut, self.spinpol)
            elf_g = self.finegd.collect(elf_g, broadcast)
            if pad:
                elf_g = self.finegd.zero_pad(elf_g)
            return elf_g
        else:
            raise NotImplementedError('Arbitrary refinement not implemented')


    def elf(self, nt_sg, nt_grad2_sg, taut_sg, ncut, spinpol):
        """Pseudo electron localisation function (ELF) as defined in
        Becke and Edgecombe, J. Chem. Phys., vol 92 (1990) 5397

        More comprehensive definition in
        M. Kohout and A. Savin, Int. J. Quantum Chem., vol 60 (1996) 875-882

        Arguments:
         =============== =====================================================
         ``nt_sg``       Pseudo valence density.
         ``nt_grad2_sg`` Squared norm of the density gradient.
         ``tau_sg``      Kinetic energy density.
         ``ncut``        Minimum density cutoff parameter.
         ``spinpol``     Boolean indicator for spin polarization.
         =============== =====================================================
        """

        # Fermi constant
        cF = 3.0 / 10 * (3 * pi**2)**(2.0 / 3.0)

        if spinpol:
            # Kouhut eq. (9)
            D0 = 2**(2.0/3.0) * cF * (nt_sg[0]**(5.0/3.0) + nt_sg[1]**(5.0/3.0))
            taut = taut_sg.sum(axis=0)
            D = taut - (nt_grad2_sg[0] / nt_sg[0] + nt_grad2_sg[1] / nt_sg[1]) / 8
        else:
            # Kouhut eq. (7)
            D0 = cF * nt_sg[0]**(5.0/3.0)
            taut = taut_sg[0]
            D = taut - nt_grad2_sg[0] / nt_sg[0] / 8

        elf_g = 1.0 / (1.0 + (D / D0)**2)

        if ncut is not None:
            nt = nt_sg.sum(axis=0)
            elf_g[nt < ncut] = 0.0

        return elf_g

    def get_localized_orbital_locator(self,gridrefinement=1,pad=True, broadcast=True):
        # Returns dimensionless LOL
        if gridrefinement == 1:
            elf_G = self.lol(self.density.nt_sG, self.nt_grad2_sG,
                         self.taut_sG, self.ncut, self.spinpol)
            elf_G = self.gd.collect(elf_G, broadcast)
            if pad:
                elf_G = self.gd.zero_pad(elf_G)
            return elf_G
        elif gridrefinement == 2:
            if self.nt_grad2_sg is None:
                self.interpolate()

            elf_g = self.lol(self.density.nt_sg, self.nt_grad2_sg,
                         self.taut_sg, self.ncut, self.spinpol)
            elf_g = self.finegd.collect(elf_g, broadcast)
            if pad:
                elf_g = self.finegd.zero_pad(elf_g)
            return elf_g
        else:
            raise NotImplementedError('Arbitrary refinement not implemented')

    def lol(self, nt_sg, nt_grad2_sg, taut_sg, ncut, spinpol):
        '''Localized Orbital Locator (LOL) as defined in
        DOI: 10.1063/1.1431271'''
        if spinpol:
           dens = (nt_sg[0]**(5.0/3.0) + nt_sg[1]**(5.0/3.0))
           tau0 = 3./5. * (6 * pi**2)**(2./3.) * (dens)
           tau = taut_sg.sum(axis=0)
        else:
           dens = (nt_sg[0]**(5.0/3.0))
           tau0 = 3./10. * (3 * pi**2)**(2./3.) * (dens)
           taut = taut_sg[0]

        t = tau0/tau
        v = t/(1. + t)
        if ncut is not None:
            nt = nt_sg.sum(axis=0)
            v[nt < ncut] = 0.0
        return v

    def help(self,help_cutoff = 0.5, save_help=True,ncut=1e-4):
        '''High Electron Localization domain Population
        by Rahm as detailed in DOI: 10.1021/acs.jctc.5b00396 and
        DOI: 10.1002/cphc.201300723

        Usage: returns the HELP domain multplied by the e-density
               as a cube file. The number of electrons within the
               domains can be computed using Henkelmann's grid Bader
               analysis'''
        from ase.units import Bohr
        from ase.io import write
        # start with ELF
        eta = self.get_electronic_localization_function(gridrefinement=2)

        #Choose regions where eta >0.5 and set others to zero
        check = eta >= help_cutoff
        eta *= check

        # limit eta to regions where density > ncut
        rho = self.paw.get_all_electron_density(gridrefinement=2)*Bohr**3
        check = rho >= ncut
        eta *= check

        if save_help:
            atoms = self.paw.atoms
            help = self.gd.collect(eta,
                      broadcast = True)
            if self.gd.comm.rank == 0:
               write('help.cube', atoms, data = help)

        # check non-zero elements of help and set to unity
        check = eta >= ncut
        eta = check*1.

        # now eta=1 where ELF >= 0.5 and zero otherwise
        # now multiply by AE density to get density and number
        # electrons within ELF using Bader
        elf_basins = eta*rho
        atoms = self.paw.atoms
        help = self.gd.collect(elf_basins,
                  broadcast = True)
        if self.gd.comm.rank == 0:
           write('help_charges.cube', atoms, data = help)
